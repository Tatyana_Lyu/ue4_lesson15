#include <iostream>

using namespace std;

void MyFunction(int N, bool even)
{
    for (int i = !even; i < N; i += 2)
        cout << i << " ";
}


int main()
{
    bool even = true;

    while(true)
    {
        std::cout << "enter 'even' or 'odd'." << "\n";

        string evenOrOdd;
        cin >> evenOrOdd;

        if (0 == evenOrOdd.compare("even"))
            even = true;
        else if (0 == evenOrOdd.compare("odd"))
            even = false;
        else
        {
            cout << "You printed not even nor odd" << endl;
            continue;
        }

        int N;
        cout << "Enter N: " << endl;
        cin >> N;

        MyFunction(N, even);
        cout << endl;
        cout << "For exit press (q)" << endl;
        string q;

        cin >> q;

        if (0 == q.compare("q"))
            break;
    }

    return 0;
}